/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <wayland-client.h>
#include <wayfire/nonstd/wlroots-full.hpp>
#include <wayfire/util/log.hpp>

#include <map>
#include <memory>
#include <wayfire/plugin.hpp>

#include <wayfire/core.hpp>
#include <wayfire/view.hpp>
#include <wayfire/output.hpp>
#include <wayfire/output-layout.hpp>
#include <wayfire/render-manager.hpp>
#include <wayfire/workspace-set.hpp>
#include <wayfire/signal-definitions.hpp>
#include <wayfire/plugins/common/input-grab.hpp>

/** To read LXQt Settings */
#include <QSettings>
#include <QStringList>

#include "LXQtShell.hpp"

static QString cleanPath( QString path );

static void configureViewAsPanel( wayfire_toplevel_view, wf::output_t * );

void LXQt::Shell::PluginImpl::init() {
    /** A new view was just added: Set the various properties if it's the correct view */
    wf::get_core().connect( &onViewMappedSignal );

    /** Configure the LXQt clients as Shell Components */
    output->connect( &onViewMappedSignal );

    /** Unset the Shell component status and clear the pointers */
    output->connect( &onViewVanishedSignal );

    /** Do not focus notification views */
    wf::get_core().connect( &onViewFocusRequest );

    QString panelPath = cleanPath( panel_config.value().length() ? QString( panel_config.value().c_str() ) : defPanelPath );
    panelCfg = new QSettings( panelPath, QSettings::IniFormat );
    QString runnerPath = cleanPath( runner_config.value().length() ? QString( runner_config.value().c_str() ) : defRunnerPath );
    runnerCfg = new QSettings( runnerPath, QSettings::IniFormat );
    QString notifyPath = cleanPath( notify_config.value().length() ? QString( notify_config.value().c_str() ) : defNotifyPath );
    notifyCfg = new QSettings( notifyPath, QSettings::IniFormat );

    panel_config.set_callback(
        [ = ] () {
            QString panelPath = (panel_config.value().length() ? QString( panel_config.value().c_str() ) : defPanelPath);
            panelCfg          = new QSettings( panelPath, QSettings::IniFormat );

            /** If panel is running on this output, reposition it */
            if ( panels[ output ].viewTop ) {
                setViewAsPanel( panels[ output ].viewTop, output );
            }
        }
    );

    runner_config.set_callback(
        [ = ] () {
            QString runnerPath = (runner_config.value().length() ? QString( runner_config.value().c_str() ) : defRunnerPath);
            runnerCfg          = new QSettings( runnerPath, QSettings::IniFormat );

            /** If runner is running on this output, reposition it */
            if ( mRunnerView ) {
                showRunner( mRunnerView, output );
            }
        }
    );

    notify_config.set_callback(
        [ = ] () {
            QString notifyPath = (notify_config.value().length() ? QString( notify_config.value().c_str() ) : defNotifyPath);
            notifyCfg          = new QSettings( notifyPath, QSettings::IniFormat );

            /** If notify is running on this output, reposition it */
            if ( mNotifyView ) {
            }
        }
    );

    /** define lxqt-commands */
    std::string startsession       = "lxqt-session";
    std::string startdesktop       = "pcmanfm-qt --desktop";
    std::string desktopprofile     = desktop_profile.value();
    std::string startpolicykit     = "lxqt-policykit-agent";
    std::string startnotificationd = "lxqt-notificationd";
    std::string startpowermanager  = "sleep 5 && lxqt-powermanagement";
    std::string startrunner        = "lxqt-runner";
    std::string startpanel         = "lxqt-panel";
    std::string panelconfig        = panel_config.value();

    if ( panelconfig.empty() ) {
        startpanel = "lxqt-panel";
    }

    else {
        startpanel = "lxqt-panel -c " + panel_config.value();
    }

    startdesktop = "pcmanfm-qt --desktop -p " + desktop_profile.value();

    /** If the user wants us to start the session */
    if ( start_session.value() ) {
        wf::get_core().run( std::string( startsession ) );
    }

    /** Otherwise start enabled LXQt modules only */
    else{
        if ( start_desktop.value() ) {
            wf::get_core().run( std::string( startdesktop ) );
        }

        if ( start_policykit.value() ) {
            wf::get_core().run( std::string( startpolicykit ) );
        }

        if ( start_notificationd.value() ) {
            wf::get_core().run( std::string( startnotificationd ) );
        }

        if ( start_powermanager.value() ) {
            wf::get_core().run( std::string( startpowermanager ) );
        }

        if ( start_runner.value() ) {
            wf::get_core().run( std::string( startrunner ) );
        }

        if ( start_panel.value() ) {
            wf::get_core().run( std::string( startpanel ) );
        }
    }
}


void LXQt::Shell::PluginImpl::fini() {
    if ( backgrounds[ output ] ) {
        backgrounds[ output ]->close();
    }

    if ( panels[ output ].viewTop ) {
        panels[ output ].viewTop->close();
    }

    if ( panels[ output ].viewLeft ) {
        panels[ output ].viewLeft->close();
    }

    if ( panels[ output ].anchorTop ) {
        output->workarea->remove_reserved_area( panels[ output ].anchorTop.get() );
        panels[ output ].anchorTop = nullptr;
    }

    if ( panels[ output ].anchorLeft ) {
        output->workarea->remove_reserved_area( panels[ output ].anchorLeft.get() );
        panels[ output ].anchorLeft = nullptr;
    }

    delete panelCfg;
    delete runnerCfg;
    delete notifyCfg;
}


void LXQt::Shell::PluginImpl::setViewAsBackground( wayfire_toplevel_view view, wf::output_t *output ) {
    backgrounds[ output ] = view;

    // view->set_decoration( nullptr );

    /* Move to the respective output */
    wf::move_view_to_output( view, output, false );

    /* A client should be used that can be resized to any size.
     * If we set it fullscreen, the screensaver would be inhibited
     * so we send a resize request that is the size of the output
     */
    view->set_geometry( output->get_relative_geometry() );

    /* Set it as the background */
    view->get_wset()->remove_view( view );
    wf::scene::readd_front( output->node_for_layer( wf::scene::layer::BACKGROUND ), view->get_root_node() );

    /* Make it show on all workspaces in Expo, Cube, etc. */
    view->sticky = true;
    view->role   = wf::VIEW_ROLE_DESKTOP_ENVIRONMENT;
}


void LXQt::Shell::PluginImpl::setViewAsPanel( wayfire_toplevel_view view, wf::output_t *output ) {
    // view->set_decoration( nullptr );

    /* Move to the respective output */
    wf::move_view_to_output( view, output, false );

    /* A client should be used that can be resized to any size.
     * If we set it fullscreen, the screensaver would be inhibited
     * so we send a resize request that is the size of the output
     */
    // view->set_geometry(view->get_pending_geometry());

    /* Set it as the background */
    view->get_wset()->remove_view( view );
    wf::scene::readd_front( output->node_for_layer( wf::scene::layer::TOP ), view->get_root_node() );

    /* Make it show on all workspaces in Expo, Cube, etc. */
    view->sticky = true;
    view->role   = wf::VIEW_ROLE_DESKTOP_ENVIRONMENT;

    configureViewAsPanel( view, output );
    output->workarea->reflow_reserved_areas();
}


void LXQt::Shell::PluginImpl::showRunner( wayfire_toplevel_view view, wf::output_t *output ) {
    mRunnerView = view;

    // view->set_decoration( nullptr );

    /* Move to the respective output */
    wf::move_view_to_output( view, output, false );

    /* A client should be used that can be resized to any size.
     * If we set it fullscreen, the screensaver would be inhibited
     * so we send a resize request that is the size of the output
     */
    // view->set_geometry(view->get_pending_geometry());

    /* Set it as the background */
    view->get_wset()->remove_view( view );
    wf::scene::readd_front( output->node_for_layer( wf::scene::layer::TOP ), view->get_root_node() );

    /* Make it show on all workspaces in Expo, Cube, etc. */
    view->sticky = true;
    view->role   = wf::VIEW_ROLE_DESKTOP_ENVIRONMENT;


    /** Get the available geometry of the current workspace of the current output */
    auto workarea = view->get_output()->workarea->get_workarea();

    /** Get the geometry of the view */
    auto window = view->get_pending_geometry();

    /** Get the horizontal center */
    window.x = workarea.x + (workarea.width / 2) - (window.width / 2);

    /** Sync before reading the settings */
    runnerCfg->sync();

    /** Slightly below the top of the workspace */
    if ( runnerCfg->value( "dialog/show_on_top" ).toBool() ) {
        window.y = workarea.y + (workarea.height / 7);
    }

    /** Center of the workspace */
    else {
        window.y = workarea.y + (workarea.height / 2) - (window.height / 2);
    }

    /** Move the view */
    view->set_geometry( window );
}


void LXQt::Shell::PluginImpl::showNotification( wayfire_toplevel_view view, wf::output_t *output ) {
    /** We need this only when geometry is modified externally. */
    onNotifyViewResized.disconnect();

    mNotifyView = view;


    // view->set_decoration( nullptr );

    /* Move to the respective output */
    wf::move_view_to_output( view, output, false );

    /* A client should be used that can be resized to any size.
     * If we set it fullscreen, the screensaver would be inhibited
     * so we send a resize request that is the size of the output
     */
    // view->set_geometry(view->get_pending_geometry());

    /* Set it as the background */
    view->get_wset()->remove_view( view );
    wf::scene::readd_front( output->node_for_layer( wf::scene::layer::TOP ), view->get_root_node() );

    /* Make it show on all workspaces in Expo, Cube, etc. */
    view->sticky = true;
    view->role   = wf::VIEW_ROLE_DESKTOP_ENVIRONMENT;

    /** Sync before reading the settings */
    notifyCfg->sync();

    /** Get the available geometry of the current workspace of the current output */
    auto workarea = view->get_output()->workarea->get_workarea();

    /** Get the geometry of the view */
    auto window = view->get_pending_geometry();

    /** Get the position */
    QString notifyPos = notifyCfg->value( "placement", "top-right" ).toString();

    if ( notifyPos == "top-center" ) {
        window.x = workarea.x + (workarea.width - window.width) / 2;
        window.y = workarea.y + 10;
    }

    else if ( notifyPos == "top-left" ) {
        window.x = workarea.x + 10;
        window.y = workarea.y + 10;
    }

    else if ( notifyPos == "center-left" ) {
        window.x = workarea.x + 10;
        window.y = workarea.y + (workarea.height - window.height) / 2;
    }

    else if ( notifyPos == "bottom-left" ) {
        window.x = workarea.x + 10;
        window.y = workarea.y + workarea.height - window.height - 10;
    }

    else if ( notifyPos == "bottom-center" ) {
        window.x = workarea.x + (workarea.width - window.width) / 2;
        window.y = workarea.y + workarea.height - window.height - 10;
    }

    else if ( notifyPos == "bottom-right" ) {
        window.x = workarea.x + workarea.width - window.width - 10;
        window.y = workarea.y + workarea.height - window.height - 10;
    }

    else if ( notifyPos == "center-right" ) {
        window.x = workarea.x + workarea.width - window.width - 10;
        window.y = workarea.y + (workarea.height - window.height) / 2;
    }

    else {  /** top-right */
        window.x = workarea.x + workarea.width - window.width - 10;
        window.y = workarea.y + 10;
    }

    /** Move the view */
    view->set_geometry( window );

    /** Now that modification is done, let's reconnect the signal */
    view->connect( &onNotifyViewResized );
}


DECLARE_WAYFIRE_PLUGIN( wf::per_output_plugin_t<LXQt::Shell::PluginImpl> );

/**
 * CODE TO SET PANEL EXCLUSIVE ZONE
 */
void configureViewAsPanel( wayfire_toplevel_view view, wf::output_t *output ) {
    /** Get the available geometry of the current workspace of the current output */
    auto workarea = output->workarea->get_workarea();

    /** Get the geometry of the view */
    auto window = view->get_pending_geometry();

    /** Horizontal panel: pin to top */
    if ( window.width > window.height ) {
        /** Ensure panel is of proper width */
        if ( window.width > workarea.width ) {
            window.width = workarea.width;
        }

        window.x = workarea.x + (workarea.width - window.width) / 2;
        window.y = workarea.y;
        panels[ output ].viewTop = view;

        if ( panels[ output ].anchorTop == nullptr ) {
            panels[ output ].anchorTop           = std::unique_ptr<wf::output_workarea_manager_t::anchored_area>();
            panels[ output ].anchorTop->reflowed =
                [ view, output ] (auto) {
                    configureViewAsPanel( view, output );
                };
            output->workarea->add_reserved_area( panels[ output ].anchorTop.get() );
        }

        panels[ output ].anchorTop->edge          = wf::output_workarea_manager_t::ANCHORED_EDGE_TOP;
        panels[ output ].anchorTop->reserved_size = window.height;
    }

    /** Horizontal panel: pin to left */
    else {
        /** Ensure panel is of proper height */
        if ( window.height > workarea.height ) {
            window.height = workarea.height;
        }

        window.x = workarea.x;
        window.y = workarea.y + (workarea.height - window.height) / 2;
        panels[ output ].viewLeft = view;

        if ( panels[ output ].anchorLeft == nullptr ) {
            panels[ output ].anchorLeft           = std::unique_ptr<wf::output_workarea_manager_t::anchored_area>();
            panels[ output ].anchorLeft->reflowed =
                [ view, output ] (auto) {
                    configureViewAsPanel( view, output );
                };
            output->workarea->add_reserved_area( panels[ output ].anchorLeft.get() );
        }

        panels[ output ].anchorLeft->edge          = wf::output_workarea_manager_t::ANCHORED_EDGE_LEFT;
        panels[ output ].anchorLeft->reserved_size = window.width;
    }

    /** Set view geometry */
    view->set_geometry( window );
}


/**
 * Replace shell variables and shortcuts with proper paths
 */
QString cleanPath( QString path ) {
    QStringList parts = path.split( "/", Qt::KeepEmptyParts );

    if ( parts.count() ) {
        /** Env variable */
        if ( parts.at( 0 ).startsWith( "$" ) ) {
            parts[ 0 ] = qEnvironmentVariable( parts.at( 0 ).toUtf8().constData(), "" );
        }

        /** Shell shortcut */
        else if ( parts.at( 0 ) == "~" ) {
            parts[ 0 ] = QDir::homePath();
        }

        /** Not an absolute path? Let's assume it's relative to pwd */
        else if ( parts.at( 0 ) != "" ) {
            parts.prepend( QDir::currentPath() );
        }

        return parts.join( "/" );
    }

    return path;
}
