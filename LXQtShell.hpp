/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <wayland-client.h>
#include <wayfire/nonstd/wlroots-full.hpp>
#include <wayfire/util/log.hpp>

#include <memory>
#include <wayfire/plugin.hpp>

#include <wayfire/output.hpp>
#include <wayfire/core.hpp>
#include <wayfire/workarea.hpp>
#include <wayfire/toplevel-view.hpp>
#include <wayfire/view-helpers.hpp>
#include <wayfire/output-layout.hpp>
#include <wayfire/render-manager.hpp>
#include <wayfire/workspace-set.hpp>
#include <wayfire/signal-definitions.hpp>

#include <wayfire/plugin.hpp>
#include <wayfire/per-output-plugin.hpp>

#include <QDir>
#include <QString>

namespace LXQt {
    namespace Shell {
        class PluginImpl;
    }
}

struct PanelView {
    /** Panel on the top edge */
    wayfire_toplevel_view                                         viewTop;
    /** Panel on the left edge */
    wayfire_toplevel_view                                         viewLeft;
    /**  */
    std::unique_ptr<wf::output_workarea_manager_t::anchored_area> anchorTop;
    /**  */
    std::unique_ptr<wf::output_workarea_manager_t::anchored_area> anchorLeft;
};

static std::map<wf::output_t *, wayfire_toplevel_view> backgrounds;
static std::map<wf::output_t *, PanelView>             panels;

class LXQt::Shell::PluginImpl : public wf::per_output_plugin_instance_t {
    public:
        void init() override;
        void fini() override;

    private:
        void setViewAsBackground( wayfire_toplevel_view, wf::output_t *output );
        void setViewAsPanel( wayfire_toplevel_view, wf::output_t *output );
        void showRunner( wayfire_toplevel_view, wf::output_t *output );
        void showNotification( wayfire_toplevel_view, wf::output_t *output );

        wayfire_toplevel_view mLastFocusView;

        wayfire_toplevel_view mRunnerView;
        wayfire_toplevel_view mNotifyView;

        /** LXQt settings */
        QSettings *panelCfg  = nullptr;
        QSettings *runnerCfg = nullptr;
        QSettings *notifyCfg = nullptr;

        wf::option_wrapper_t<bool> start_session{ "lxqt-shell/start_lxqt_session" };
        wf::option_wrapper_t<bool> start_desktop{ "lxqt-shell/start_desktop" };
        wf::option_wrapper_t<bool> start_policykit{ "lxqt-shell/start_policykit" };
        wf::option_wrapper_t<bool> start_notificationd{ "lxqt-shell/start_notificationd" };
        wf::option_wrapper_t<bool> start_powermanager{ "lxqt-shell/start_powermanager" };
        wf::option_wrapper_t<bool> start_runner{ "lxqt-shell/start_runner" };
        wf::option_wrapper_t<bool> start_panel{ "lxqt-shell/start_panel" };

        wf::option_wrapper_t<std::string> desktop_profile{ "lxqt-shell/desktop_profile" };
        wf::option_wrapper_t<std::string> panel_config{ "lxqt-shell/panel_config_file" };
        // obsolete:
        wf::option_wrapper_t<std::string> runner_config{ "lxqt-shell/runner_config_file" };
        wf::option_wrapper_t<std::string> notify_config{ "lxqt-shell/notify_config_file" };

        QString defPanelPath  = QDir::home().filePath( ".config/lxqt/panel.conf" );
        QString defRunnerPath = QDir::home().filePath( ".config/lxqt/lxqt-runner.conf" );
        QString defNotifyPath = QDir::home().filePath( ".config/lxqt/notifications.conf" );

        /** Will be used to set the role of notification view only */
        wf::signal::connection_t<wf::view_mapped_signal> onViewMappedSignal =
            [ = ] (wf::view_mapped_signal *ev) {
                /** A view opened on a different output will be handled by that instance of this plugin */
                if ( ev->view->get_output() != output ) {
                    return;
                }

                /* We're interested only in top-levels */
                if ( ev->view->role != wf::VIEW_ROLE_TOPLEVEL ) {
                    return;
                }

                wayfire_toplevel_view view  = wf::toplevel_cast( ev->view );
                std::string           appId = view->get_app_id();

                /**
                 * Set the role of the notification view as DE.
                 * This way, it will not interfere show-desktop of wm-actions.
                 */
                if ( ev->view->get_app_id() == "lxqt-notificationd" ) {
                    if ( ev->view->get_title() == "lxqt-notificationd" ) {
                        ev->view->role = wf::VIEW_ROLE_DESKTOP_ENVIRONMENT;
                    }
                }

                /** PCManFM Qt: Probably started in desktop mode */
                else if ( appId == "pcmanfm-qt" ) {
                    std::string title = view->get_title();

                    if ( (title == "pcmanfm-qt") or (title == "pcmanfm-desktop0") ) {
                        for ( auto& op : wf::get_core().output_layout->get_outputs() ) {
                            if ( backgrounds[ op ] ) {
                                continue;
                            }

                            setViewAsBackground( view, op );
                            ev->is_positioned = true;

                            return;
                        }
                    }
                }

                /** PCManFM Qt: Probably started in desktop mode */
                else if ( appId == "lxqt-panel" ) {
                    std::string title = view->get_title();

                    if ( title == "LXQt Panel" ) {
                        for ( auto& op : wf::get_core().output_layout->get_outputs() ) {
                            if ( panels[ op ].viewTop and panels[ op ].viewLeft ) {
                                continue;
                            }

                            setViewAsPanel( view, op );
                            ev->is_positioned = true;

                            return;
                        }
                    }
                }

                /** PCManFM Qt: Probably started in desktop mode */
                else if ( appId == "lxqt-runner" ) {
                    std::string title = view->get_title();

                    if ( title == "LXQt Runner" ) {
                        wf::output_t *output = wf::get_core().seat->get_active_output();

                        if ( output != nullptr ) {
                            showRunner( view, output );
                            ev->is_positioned = true;
                        }
                    }
                }

                /** PCManFM Qt: Probably started in desktop mode */
                else if ( appId == "lxqt-notificationd" ) {
                    std::string title = view->get_title();

                    if ( title == "lxqt-notificationd" ) {
                        wf::output_t *output = wf::get_core().seat->get_active_output();

                        if ( output != nullptr ) {
                            showNotification( view, output );
                            ev->is_positioned = true;
                        }
                    }
                }
            };

        /** Used */
        wf::signal::connection_t<wf::view_disappeared_signal> onViewVanishedSignal =
            [ = ] ( wf::view_disappeared_signal *ev ) {
                if ( ev->view == nullptr ) {
                    return;
                }

                if ( ev->view->role != wf::VIEW_ROLE_TOPLEVEL ) {
                    return;
                }

                wayfire_toplevel_view view = wf::toplevel_cast( ev->view );

                if ( backgrounds[ output ] == view ) {
                    backgrounds[ output ] = nullptr;
                }

                if ( ev->view and panels[ output ].viewTop == view ) {
                    panels[ output ].viewTop = nullptr;

                    if ( panels[ output ].anchorTop ) {
                        output->workarea->remove_reserved_area( panels[ output ].anchorTop.get() );
                        panels[ output ].anchorTop = nullptr;
                        output->workarea->reflow_reserved_areas();
                    }
                }

                if ( ev->view and panels[ output ].viewLeft == view ) {
                    panels[ output ].viewLeft = nullptr;

                    if ( panels[ output ].anchorLeft ) {
                        output->workarea->remove_reserved_area( panels[ output ].anchorLeft.get() );
                        panels[ output ].anchorLeft = nullptr;
                        output->workarea->reflow_reserved_areas();
                    }
                }

                if ( view == mLastFocusView ) {
                    mLastFocusView = nullptr;
                }

                if ( view == mRunnerView ) {
                    mRunnerView = nullptr;
                }

                if ( view == mNotifyView ) {
                    mNotifyView = nullptr;
                }
            };

        /** Used to disable focus of notification views */
        wf::signal::connection_t<wf::view_focus_request_signal> onViewFocusRequest =
            [ = ] (wf::view_focus_request_signal *ev) {
                if ( ev->view and ev->view->role == wf::VIEW_ROLE_TOPLEVEL ) {
                    std::string appId = ev->view->get_app_id();
                    std::string title = ev->view->get_title();

                    if ( wf::toplevel_cast( ev->view ) == mNotifyView ) {
                        ev->carried_out = true;

                        if ( mLastFocusView ) {
                            /** May be show-dessktop is active. */

                            /** Source 1: wm-actions */
                            if ( mLastFocusView->has_data( "wm-actions-showdesktop" ) ) {
                                return;
                            }

                            /** Source 2: dbusqt plugin */
                            if ( mLastFocusView->has_data( "dbusqt-showdesktop" ) ) {
                                return;
                            }

                            /** Source 3: wayfire-workspaces-unstable-v1 */
                            if ( mLastFocusView->has_data( "wf-workspaces-showdesktop" ) ) {
                                return;
                            }

                            /** Show-desktop is not active. So refocus the last view */
                            wf::view_bring_to_front( mLastFocusView );
                        }
                    }

                    else {
                        mLastFocusView = wf::toplevel_cast( ev->view );
                    }
                }
            };

        /** To reposition the notification views */
        wf::signal::connection_t<wf::view_geometry_changed_signal> onNotifyViewResized =
            [ = ] (wf::view_geometry_changed_signal *ev) {
                if ( ev->view and (ev->view == mNotifyView) ) {
                    showNotification( ev->view, output );
                }
            };
};
